import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CalculatorProvider } from '../../providers/calculator/calculator';
import { AdProvider } from '../../providers/ad/ad';

import swal from 'sweetalert';
import $ from 'jquery';
import { AdvancedModel } from '../../models/AdvancedModel';

@Component({
  selector: 'page-advanced',
  templateUrl: 'advanced.html',
})
export class AdvancedPage implements OnInit {

  firstNumberOfPeople: number;
  secondNumberOfPeople: number;
  secondTaxiFare: number;
  firstTaxiFare: number;
  moneyPaid: number;
  change: number;
  title: string;
  subTitle: string;

  advanced : AdvancedModel;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private prov: CalculatorProvider, private adProv: AdProvider) {
    this.adMob();
  }

  ngOnInit() {
  }

  adMob() {
    this.adProv.showBannerAd();
    this.adProv.showInterstitialAd();
  }

  complexCalculation() {

    this.advanced = {
      firstNoPeople: this.firstNumberOfPeople,
      firstTaxiFare : this.firstTaxiFare,
      secondNoPeople: this.secondNumberOfPeople,
      secondTaxiFare: this.secondTaxiFare
    }

    if (this.firstTaxiFare === undefined || this.firstTaxiFare === 0 || this.firstTaxiFare < 2) {
      this.title = 'First Row';
      this.subTitle = 'Please provide a valid taxi fare.'
      this.showAlert(title, subTitle);
    }
    else if (this.firstNumberOfPeople === undefined || this.firstNumberOfPeople === 0) {
      this.title = 'First Row';
      this.subTitle = 'Please provide a valid number of people.'
      this.showAlert(title, subTitle);
    }
    else if (this.secondTaxiFare === undefined || this.secondTaxiFare === 0 || this.secondTaxiFare < 2) {
      this.title = 'Second Row';
      this.subTitle = 'Please provide a valid taxi fare.'
      this.showAlert(title, subTitle);
    }
    else if (this.secondNumberOfPeople === undefined || this.secondNumberOfPeople === 0) {
      this.title = 'Second Row';
      this.subTitle = 'Please provide a valid number of people.'
      this.showAlert(title, subTitle);
    }
    else if (this.moneyPaid === undefined || this.moneyPaid === 0) {
      var title = 'Amount Paid';
      var subTitle = 'Please provide a valid amount.'
      this.showAlert(title, subTitle);
    }
    else if (this.moneyPaid < 2) {
      this.title = 'Amount Paid';
      this.subTitle = 'The aount provided is not enough. \nPlease provide enough amount.'
      this.showAlert(title, subTitle);
    }
    else {
      var sum = this.prov.advancedCalculator(this.advanced);

      if (sum > this.moneyPaid) {
        var convertChange = Math.abs(this.moneyPaid - sum);
        this.showAlert('Change', 'R' + convertChange + ' short!');
      } else {
        this.change = this.moneyPaid - sum;
        this.title = 'Change';
        this.subTitle = 'R' + this.change + ' change';
        swal(title, subTitle, 'success');
      }

    }
  }

  inputOnChange() {
    $(document).ready(function () {
      $('.form-control').each(function () {
        if ($(this).val().toString().length > 0) {
          $(this).addClass('has-value');
        } else {
          $(this).removeClass('has-value');
        }
      });

      $('.form-control').on('focusOut', function () {
        if ($(this).val().toString().length > 0) {
          $(this).addClass('has-value');
        } else {
          $(this).removeClass('has-value');
        }
      });
    });
  }

  showAlert(alertTitle: string, alertSubTitle: string) {
    swal(alertTitle,
      alertSubTitle,
      'warning');
  }

}
