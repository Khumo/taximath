import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NavController, NavParams, Range } from 'ionic-angular';

import swal from 'sweetalert';
import $ from 'jquery';

import { CalculatorProvider } from '../../providers/calculator/calculator';
import { AdProvider } from '../../providers/ad/ad';
import { BasicModel } from '../../models/BasicModel';

@Component({
  selector: 'page-basic',
  templateUrl: 'basic.html',
})
export class BasicPage implements OnInit {

  change: number;
  numberOfPeople: number;
  moneyPaid: number;
  taxiFare: number;
  test: number;

  basic: BasicModel;

  @Input() max: number;
  @Input() min: number;
  //@Input() pin: boolean;
  @Input() step: number = 1;
  @Input() snaps: boolean;

  @ViewChild('range')
  set range(range: Range) {
    let floatRange: BasicPage = this;
    range._ratioToValue = function (ratio: number) {
      this._step = Math.round(floatRange.step * 100) / 100;
      ratio = (this._max - this._min) * ratio;
      ratio = (ratio / this._step) * this._step + this._min;
      return Math.round(ratio * (1 / this._step)) / (1 / this._step);
    }
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private prov: CalculatorProvider, private adProv: AdProvider) {
    this.adMob();
  }

  ngOnInit() {
    this.numberOfPeople = 3;
    this.taxiFare = 8.50;
  }

  adMob() {
    this.adProv.showBannerAd();
    this.adProv.showInterstitialAd();
  }

  calculateChange() {

    this.basic = {
      noPeople: this.numberOfPeople,
      taxiFare: this.taxiFare
    }

    if (this.moneyPaid === undefined || this.moneyPaid === 0) {
      this.showAlert('Amount Paid', 'Please provide a valid amount.');
    }
    else if (this.moneyPaid < 6) {
      this.showAlert('Amount Paid', 'The money provided is not enough.\n\t\t\tPlease provide enough money.');
    }
    else {
      var sum = this.prov.basicCalculator(this.basic);
      if (sum > this.moneyPaid) {
        this.change = Math.abs(this.moneyPaid - sum);
        this.showAlert('Change', 'R' + this.change.toFixed(2) + ' short!');
      } else {
        this.change = this.moneyPaid - sum;
        swal('Change', 'R' + this.change.toFixed(2) + ' change!', 'success');
      }
    }
  }

  showAlert(alertTitle: string, alertSubTitle: string) {
    swal(alertTitle,
      alertSubTitle,
      'warning');
  }

  inputOnChange() {
    $(document).ready(function () {
      $('.form-control').each(function () {
        if ($(this).val().toString().length > 0) {
          $(this).addClass('has-value');
        } else {
          $(this).removeClass('has-value');
        }
      });

      $('.form-control').on('focusOut', function () {
        if ($(this).val().toString().length > 0) {
          $(this).addClass('has-value');
        } else {
          $(this).removeClass('has-value');
        }
      });
    });
  }

}
