import { Injectable } from '@angular/core';

import { AdMobFree, AdMobFreeInterstitialConfig, AdMobFreeBannerConfig, AdMobFreeRewardVideoConfig } from '@ionic-native/admob-free';

@Injectable()
export class AdProvider {

  constructor(private adMobFree: AdMobFree) {
  }

  async showBannerAd() {
    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-1373180569835658/8367139138',
      isTesting: true,
      autoShow: true
    }

    this.adMobFree.banner.config(bannerConfig);

    try {
      const result = await this.adMobFree.banner.prepare();
      console.log(result);
    } catch (error) {
      console.error(error);
    }
  }

  async showInterstitialAd() {
    try {
      const interstitialConfig: AdMobFreeInterstitialConfig = {
        id: 'ca-app-pub-1373180569835658/3693396958',
        isTesting: true,
        autoShow: true
      }

      this.adMobFree.interstitial.config(interstitialConfig);

      const result = await this.adMobFree.interstitial.prepare();
      console.log(result);
    } catch (error) {
      console.error(error);
    }
  }

  async showVideoRewardsAd() {
    try {
      const videoRewardsConfig: AdMobFreeRewardVideoConfig = {
        id: 'ca-app-pub-1373180569835658~6646863357',
        isTesting: true,
        autoShow: true
      }

      this.adMobFree.rewardVideo.config(videoRewardsConfig);
      const result = await this.adMobFree.rewardVideo.prepare();
      console.log(result);
    } catch (error) {
      console.error(error)
    }
  }
}
