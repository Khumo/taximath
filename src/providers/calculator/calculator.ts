import { Injectable } from '@angular/core';

import { BasicModel } from '../../models/BasicModel';
import { AdvancedModel } from '../../models/AdvancedModel';

@Injectable()
export class CalculatorProvider {

  constructor() {
  }

  basicCalculator(basic: BasicModel) {
    return basic.noPeople * basic.taxiFare;
  }

  advancedCalculator(advanced: AdvancedModel) {
    return (advanced.firstNoPeople * advanced.firstTaxiFare) + (advanced.secondNoPeople * advanced.secondTaxiFare);
  }

}
